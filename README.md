Complex discrete dynamical system based on the polynomial map f

# polynomial map
Map f with 2 complex coefficients A and c described by [Marc Meidlinger](https://fractalforums.org/image-threads/25/julia-and-mandelbrot-sets-w-or-wo-lyapunov-sequences/2696/msg20202#msg20202)

f(z)=z^6 +A*x+ c  


## critical points


The degree d of polynomial is 6 so there is d-1 = 5 [critical points](https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/def_cqp#Critical_3): 


Here symbolic computation with Maxima CAS: 
```
Maxima 5.43.2 http://maxima.sourceforge.net
using Lisp GNU Common Lisp (GCL) GCL 2.6.12
Distributed under the GNU Public License. See the file COPYING.
Dedicated to the memory of William Schelter.
The function bug_report() provides bug reporting information.
(%i4) display2d:false;

(%o4) false
(%i5) f(z):=z^6+A*z+c;

(%o5) f(z):=z^6+A*z+c
(%i6) d:diff(f(z),z,1);

(%o6) 6*z^5+A

(%i8) solve(d=0,z);

(%o8) [z = -(%e^((2*%i*%pi)/5)*A^(1/5))/6^(1/5),
       z = -(%e^((4*%i*%pi)/5)*A^(1/5))/6^(1/5),
       z = -(%e^-((4*%i*%pi)/5)*A^(1/5))/6^(1/5),
       z = -(%e^-((2*%i*%pi)/5)*A^(1/5))/6^(1/5),
       z = -A^(1/5)/6^(1/5)]
```


or [WolframAlfa](https://www.wolframalpha.com/input/?i=critical+points+of+z%5E6%2B%280.06968548893928528-0.1079580187797546*i%29*z-0.5703125*i%2B0.46875)

So critical point:
* depend on A = must be computed for each A value
* do not depend on c = are the same for all A values




# dynamic plane

Here: 

A = (2338257-3622470*i) * (2^-25) = 0.0696854889392852783203125 - 0.107958018779754638671875 i   
c = (15728640-19136512*i) * (2^-25) = 0.46875 - 0.5703125 i   

Values were computed with [WolframAlfa](https://www.wolframalpha.com/input/?i=%282338257-3622470*i%29+*+%282%5E-25%29)



## critical points:

Here all 5 critical points are: 
* +0.4216319827875524	+0.1927564710317439*i
* -0.4544068504035357	+0.09188580053693407*i
* -0.2278080284907348 	-0.4037723222177803*i
* +0.3136137458861571	-0.3414308193839966*i
* -0.05303084977943909	+0.4605608700330989*i

It was computed with [WolframAlfa](https://www.wolframalpha.com/input/?i=critical+points+of+z%5E6%2B%280.06968548893928528-0.1079580187797546*i%29*z-0.5703125*i%2B0.46875)


## critical orbits

Forward orbit of critical point is called [critical orbit](https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/def_cqp#Critical_2).  
Critical orbits are very important because every attracting periodic orbit attracts a critical point, so studying the critical orbits helps us understand the dynamics in the Fatou set.


![400_i.png](./images/400_i.png)


It was made with [c.mac](./src/c.mac)

## atracting periodic points

According to [Marc Meidlinger](https://fractalforums.org/image-threads/25/julia-and-mandelbrot-sets-w-or-wo-lyapunov-sequences/2696/msg20202#msg20202) period of the limit cycle is 78





## image

Image of dynamic plane with filled-in Julia set:  

![](./images/d.png)

It was made with  [d.c](./src/d.c). See also [commons verions](https://commons.wikimedia.org/wiki/File:Julia_set_for_fc(z)%3D_z%5E6%2BA*z%2Bc_where_c_%3D_4.6875e-1_-_5.703125e-1_*I_and_A_%3D_6.96854889392852783203125e-2_-_1.07958018779754638671875e-1*I.png) 


# Parameter space

The polynomial has 2 complex coefficients: A and c. So [the parameter space](https://en.wikipedia.org/wiki/Parameter_space) is 4 dimensional.   



Mandelbrot set defined as [a connectedness locus](https://en.wikipedia.org/wiki/Connectedness_locus) is a set of parameters for which orbits of all criticall points are connected.







It is possible to draw 2 dimensional slices of parameter space.



## A-planes 
 
Here A is changing and c is fixed:

c = (15728640-19136512*i) * (2^-25) = 0.46875 - 0.5703125 i      

## c-planes 

Here c is changing and A is fixed:  

A = (2338257-3622470*i) * (2^-25) = 0.0696854889392852783203125 - 0.107958018779754638671875 i   

2D parameter c-planes for each critical points: 

![](./images/0.png)

![](./images/1.png)

![](./images/2.png)

![](./images/3.png)

![](./images/4.png)


Above images are not the same  


images made with [l.c](./src/l.c)


Mandelbrot set : if and only if all critical orbits are bounded then point is inside Mandelbrot set. So it is [an intersection](https://en.wikipedia.org/wiki/Intersection_(set_theory)) of all above sets.

$`\bigcap_{i=1}^n M_i  `$ 




![](./images/1d10000.png)  



```c

unsigned char ComputeColor (complex double c, int IterMax)
{

  int ci; // index of critical array
  complex double z; // = critical[2];
  int i; // number of iteration
  
  
  for (ci = 0; ci < 5; ++ci) // for each critical point 
    {
    	z = critical[ci]; // read critical point number ci 
  	for (i = 0; i < IterMax; ++i)
    		{

			z = z*z*z*z*z*z +A*z+ c;		// complex iteration z^6+A*z+c

      			if (cabs2 (z) > ER2) // escaping
				{  // unbounded orbit of any critical point = exterior 
				  uExterior += 1;
	  			  return iColorOfExterior;
				}			




    		} //for i
    		
    	} // for ci
  //  non escaping = bonded orbits =  if and only if all critical orbits are bounded = interior
  uInterior += 1;
  return iColorOfInterior;


}



```




## zoom 



![](./images/0d100000.png)  

![](./images/0d010000.png)  

![](./images/0d000100.png)  

![](./images/0d000001.png)  
 
 
Name of the file  is radius ( d means decimal dot )   
On the last image dot showing c values is outside the set ( = error)  
 
All images made with [z.c](./src/z.c)   

# Git

```git
cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/labiryth.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

clone 

```git
git clone git@gitlab.com:adammajewski/labiryth.git
```


Subdirectory

```git
mkdir images
git add *.png
git mv  *.png ./images
git commit -m "move"
git push -u origin master
```

local repo: ~/Dokumenty/labirynth


