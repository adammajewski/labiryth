/*

  Adam Majewski
  adammaj1 aaattt o2 dot pl  // o like oxygen not 0 like zero 
  
  
  


  
  ==============================================
  
  
  Structure of a program or how to analyze the program 
  
  
  ============== Image X ========================
  
  DrawImageOfX -> DrawPointOfX -> ComputeColorOfX 
  
  first 2 functions are identical for every X
  check only last function =  ComputeColorOfX
  which computes color of one pixel !
  
  

   
  ==========================================

  
  ---------------------------------
  indent d.c 
  default is gnu style 
  -------------------



  c console progam 
  
	export  OMP_DISPLAY_ENV="TRUE"	
  	gcc d.c -lm -Wall -march=native -fopenmp
  	time ./a.out > b.txt


  gcc l.c -lm -Wall -march=native -fopenmp


  time ./a.out

  time ./a.out >i.txt
  time ./a.out >e.txt
  
  
  
  
  
  
  convert -limit memory 1000mb -limit disk 1gb dd30010000_20_3_0.90.pgm -resize 2000x2000 10.png
  
  
  =============================
  
critical points : 
   
+0.4216319827875524	+0.1927564710317439*%i
-0.4544068504035357	+0.09188580053693407*%i
-0.2278080284907348 	-0.4037723222177803*%i
+0.3136137458861571	-0.3414308193839966*%i
-0.05303084977943909	+0.4605608700330989*%i
-------------------------------------------------------
+0.421632 		+ 0.192756 I  
-0.454407 		+ 0.0918858 I 
-0.227808 		- 0.403772 I 
+0.313614 		- 0.341431 I 
-0.0530308 		+ 0.460561 I 

  
  
  
  

  
  
*/

#include <stdio.h>
#include <stdlib.h>		// malloc
#include <string.h>		// strcat
#include <math.h>		// M_PI; needs -lm also
#include <complex.h>
#include <omp.h>		// OpenMP
#include <limits.h>		// Maximum value for an unsigned long long int



// https://sourceforge.net/p/predef/wiki/Standards/

#if defined(__STDC__)
#define PREDEF_STANDARD_C_1989
#if defined(__STDC_VERSION__)
#if (__STDC_VERSION__ >= 199409L)
#define PREDEF_STANDARD_C_1994
#endif
#if (__STDC_VERSION__ >= 199901L)
#define PREDEF_STANDARD_C_1999
#endif
#endif
#endif




/* --------------------------------- global variables and consts ------------------------------------------------------------ */



// virtual 2D array and integer ( screen) coordinate
// Indexes of array starts from 0 not 1 
//unsigned int ix, iy; // var
static unsigned int ixMin = 0;	// Indexes of array starts from 0 not 1
static unsigned int ixMax;	//
static unsigned int iWidth;	// horizontal dimension of array

static unsigned int iyMin = 0;	// Indexes of array starts from 0 not 1
static unsigned int iyMax;	//

static unsigned int iHeight = 1000;	//  
// The size of array has to be a positive constant integer 
static unsigned long long int iSize;	// = iWidth*iHeight; 

// memmory 1D array 
unsigned char *data;
//unsigned char *edge;
//unsigned char *edge2;

// unsigned int i; // var = index of 1D array
//static unsigned int iMin = 0; // Indexes of array starts from 0 not 1
static unsigned int iMax;	// = i2Dsize-1  = 
// The size of array has to be a positive constant integer 
// unsigned int i1Dsize ; // = i2Dsize  = (iMax -iMin + 1) =  ;  1D array with the same size as 2D array



// parameter c plane
double CxMin ; //= -2.0;	//-0.05;
double CxMax ; //= 2.0;	//0.75;
double CyMin ; //= -2.0;	//-0.1;
double CyMax ; //= 2.0;	//0.7;
// set plane from center and radius : https://en.wikibooks.org/wiki/Fractals/Computer_graphic_techniques/2D/plane#Viewport_(visible_part_of_the_plane_)
complex double center = 0.0;
double radius = 1.1;





static double PixelWidth;	// =(ZxMax-ZxMin)/ixMax;
static double PixelHeight;	// =(ZyMax-ZyMin)/iyMax;
static double ratio;
double Magnification;


// complex numbers of parametr plane 
//https://fractalforums.org/code-snippets-fragments/74/lagrangian-descriptors-fragment-code/3612/msg22426#msg22426
double complex c0 =  4.6875e-1 - 5.703125e-1 *I;	// parameter of function fc(z)=z^6 +A*x+ c  = (15728640-19136512*i) * (2^-25)
                   
//A: (2338257-3622470*i) * (2^-25)
double complex  A=  6.96854889392852783203125e-2 - 1.07958018779754638671875e-1 *I;


// 5 critical z points computed in other program
complex double critical[5] ={
   
+0.4216319827875524+0.1927564710317439*I,
-0.4544068504035357+0.09188580053693407*I,
-0.2278080284907348-0.4037723222177803*I,
+0.3136137458861571-0.3414308193839966*I,
-0.05303084977943909+0.4605608700330989*I


};

double ER;			//= 1e60;
double ER2; // ER*ER


int IterMax = 10000;



/* colors = shades of gray from 0 to 255 */
unsigned char iColorOfExterior = 250;
unsigned char iColorOfInterior = 50;
unsigned char iColorOfBoundary = 0;
unsigned char iColorOfUnknown = 255;

// pixel counters
unsigned long long int uUnknown = 0;
unsigned long long int uInterior = 0;
unsigned long long int uExterior = 0;




/* ------------------------------------------ functions -------------------------------------------------------------*/





//------------------complex numbers -----------------------------------------------------




double cabs2(complex double z){

	return creal(z)*creal(z)+cimag(z)*cimag(z);


}



// from screen to world coordinate ; linear mapping
// uses global cons
double
GiveCx (int ix)
{
  return (CxMin + ix * PixelWidth);
}

// uses globaal cons
double
GiveCy (int iy)
{
  return (CyMax - iy * PixelHeight);
}				// reverse y axis


complex double
GiveC (int ix, int iy)
{
  double Cx = GiveCx (ix);
  double Cy = GiveCy (iy);

  return Cx + Cy * I;




}




int SetPlaneFromCenterRadius(complex double center, double radius){
  
  //
  Magnification = 1.0/radius;
  double ImageRatio = ((double) iWidth / (double) iHeight); // https://en.wikipedia.org/wiki/Aspect_ratio_(image)
  
  
  CxMin = creal(center) - radius*ImageRatio ;
  CxMax = creal(center) + radius*ImageRatio;
  CyMin = cimag(center) - radius; // !!!!
  CyMax = cimag(center) + radius;
  
  
  return 0; 
}





// ****************** DYNAMICS = trap tests ( target sets) ****************************


/* -----------  array functions = drawing -------------- */

/* gives position of 2D point (ix,iy) in 1D array  ; uses also global variable iWidth */
unsigned int
Give_i (unsigned int ix, unsigned int iy)
{
  return ix + iy * iWidth;
}





unsigned char ComputeColor (complex double c, int IterMax)
{

  int ci; // index of critical array
  
  complex double z; // = critical[2];
  
  
  int i; // number of iteration
  
  
  for (ci = 0; ci < 5; ++ci)
    {
    	z = critical[ci];
  	for (i = 0; i < IterMax; ++i)
    		{

			z = z*z*z*z*z*z +A*z+ c;		// complex iteration z^6+A*z+c

      			if (cabs2 (z) > ER2) // escaping
				{
	  			uExterior += 1;
	  			return iColorOfExterior;
				}			// exterior 




    		} //for i
    	} // for ci
  //  non escaping
  uInterior += 1;
  return iColorOfInterior;


}





// plots raster point (ix,iy) 
int
DrawPoint (unsigned char A[], int ix, int iy, int IterMax)
{
  int i;			/* index of 1D array */
  unsigned char iColor = 0;
  complex double c;


  i = Give_i (ix, iy);		/* compute index of 1D array from indices of 2D array */
  c = GiveC (ix, iy);
  iColor = ComputeColor (c, IterMax);
  A[i] = iColor;		// interior

  return 0;
}




// fill array 
// uses global var :  ...
// scanning complex plane 
int
DrawImage (unsigned char A[], int IterMax)
{
  unsigned int ix, iy;		// pixel coordinate 

  printf ("compute  image \n");
  // for all pixels of image 
#pragma omp parallel for schedule(dynamic) private(ix,iy) shared(A, ixMax , iyMax, uUnknown, uInterior, uExterior)
  for (iy = iyMin; iy <= iyMax; ++iy)
    {
      printf (" %d from %d \r", iy, iyMax);	//info 
      for (ix = ixMin; ix <= ixMax; ++ix)
	DrawPoint (A, ix, iy, IterMax);	//  
    }

  return 0;
}


//=========




int IsInside (int x, int y, int xcenter, int ycenter, int r){

	
	double dx = x- xcenter;
	double dy = y - ycenter;
	double d = sqrt(dx*dx+dy*dy);
	if (d<r) 
		return 1;
	return 0;
	  

} 



int PlotPoint(complex double c, unsigned char A[]){

	
	unsigned int ix_seed = (creal(c)-CxMin)/PixelWidth;
	unsigned int iy_seed = (CyMax - cimag(c))/PixelHeight;
	unsigned int i;
	
	
	 /* mark seed point by big pixel */
  	int iSide =50.0*iWidth/2000.0 ; /* half of width or height of big pixel */
  	int iY;
  	int iX;
  	for(iY=iy_seed-iSide;iY<=iy_seed+iSide;++iY){ 
    		for(iX=ix_seed-iSide;iX<=ix_seed+iSide;++iX){ 
    			if (IsInside(iX, iY, ix_seed, iy_seed, iSide)) {
      			i= Give_i(iX,iY); /* index of _data array */
      			A[i]= 255-A[i];}}}
	
	
	return 0;
	
}















// *******************************************************************************************
// ********************************** save A array to pgm file ****************************
// *********************************************************************************************

int
SaveArray2PGMFile (unsigned char A[], int ia, int ib, int ic, double d,  char *comment)
{

  FILE *fp;
  const unsigned int MaxColorComponentValue = 255;	/* color component is coded from 0 to 255 ;  it is 8 bit color file */
  char name[100];		/* name of file */
  snprintf (name, sizeof name, "s_%d_%d_%d_%.16f", ia, ib, ic,d );	/*  */
  char *filename = strcat (name, ".pgm");
  char long_comment[200];
  sprintf (long_comment, "%s\tER = %e", comment, ER);





  // save image array to the pgm file 
  fp = fopen (filename, "wb");	// create new file,give it a name and open it in binary mode 
  fprintf (fp, "P5\n # %s\n %u %u\n %u\n", long_comment, iWidth, iHeight, MaxColorComponentValue);	// write header to the file
  fwrite (A, iSize, 1, fp);	// write array with image data bytes to the file in one step 
  fclose (fp);

  // info 
  printf ("File %s saved ", filename);
  if (long_comment == NULL || strlen (long_comment) == 0)
    printf ("\n");
  else
    printf (". Comment = %s \n", long_comment);

  return 0;
}




int
PrintCInfo ()
{

  printf ("gcc version: %d.%d.%d\n", __GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__);	// https://stackoverflow.com/questions/20389193/how-do-i-check-my-gcc-c-compiler-version-for-my-eclipse
  // OpenMP version is displayed in the console : export  OMP_DISPLAY_ENV="TRUE"

  printf ("__STDC__ = %d\n", __STDC__);
  printf ("__STDC_VERSION__ = %ld\n", __STDC_VERSION__);
  printf ("c dialect = ");
  switch (__STDC_VERSION__)
    {				// the format YYYYMM 
    case 199409L:
      printf ("C94\n");
      break;
    case 199901L:
      printf ("C99\n");
      break;
    case 201112L:
      printf ("C11\n");
      break;
    case 201710L:
      printf ("C18\n");
      break;
      //default : /* Optional */

    }

  return 0;
}


int
PrintProgramInfo ()
{


  // display info messages
  printf ("Numerical approximation of Mandelbrot set for fc(z)= z^6+A*z+c \n");
  //printf ("iPeriodParent = %d \n", iPeriodParent);
  //printf ("iPeriodOfChild  = %d \n", iPeriodChild);
  printf ("parameter c0 = ( %.16f ; %.16f ) \n", creal (c0), cimag (c0));

  printf ("Image Width = %f in world coordinate\n", CxMax - CxMin);
  printf ("PixelWidth = %.16f \n", PixelWidth);



  //printf("pixel counters\n");
  //printf ("uUnknown = %llu\n", uUnknown);
  //printf ("uExterior = %llu\n", uExterior);
  //printf ("uInterior = %llu\n", uInterior);
  //printf ("Sum of pixels  = %llu\n", uInterior+uExterior + uUnknown);
  //printf ("all pixels of the array = iSize = %llu\n", iSize);


  // image corners in world coordinate
  // center and radius
  // center and zoom
  // GradientRepetition
  printf ("Maximal number of iterations = iterMax = %d \n", IterMax);
  printf ("ratio of image  = %f ; it should be 1.000 ...\n", ratio);
  //




  return 0;
}






// *****************************************************************************
//;;;;;;;;;;;;;;;;;;;;;;  setup ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
// **************************************************************************************

int
setup ()
{

  printf ("setup start\n");






  /* 2D array ranges */

  iWidth = iHeight;
  iSize = iWidth * iHeight;	// size = number of points in array 
  // iy
  iyMax = iHeight - 1;		// Indexes of array starts from 0 not 1 so the highest elements of an array is = array_name[size-1].
  //ix

  ixMax = iWidth - 1;

  /* 1D array ranges */
  // i1Dsize = i2Dsize; // 1D array with the same size as 2D array
  iMax = iSize - 1;		// Indexes of array starts from 0 not 1 so the highest elements of an array is = array_name[size-1].
  
  
  
  SetPlaneFromCenterRadius(center,radius);
  

  /* Pixel sizes */
  PixelWidth = (CxMax - CxMin) / ixMax;	//  ixMax = (iWidth-1)  step between pixels in world coordinate 
  PixelHeight = (CyMax - CyMin) / iyMax;
  ratio = ((CxMax - CxMin) / (CyMax - CyMin)) / ((double) iWidth / (double) iHeight);	// it should be 1.000 ...

  ER = 2.0; //pow (10, ERe);
  ER2 = ER*ER;



  /* create dynamic 1D arrays for colors ( shades of gray ) */
  data = malloc (iSize * sizeof (unsigned char));


  if (data == NULL)
    {
      fprintf (stderr, " Could not allocate memory");
      return 1;
    }





 


  printf ("end of setup \n");

  return 0;

}				// ;;;;;;;;;;;;;;;;;;;;;;;;; end of the setup ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




int
end ()
{


  printf (" allways free memory (deallocate )  to avoid memory leaks \n");	// https://en.wikipedia.org/wiki/C_dynamic_memory_allocation
  free (data);


  PrintProgramInfo ();
  PrintCInfo ();
  return 0;

}

// ********************************************************************************************************************
/* -----------------------------------------  main   -------------------------------------------------------------*/
// ********************************************************************************************************************

int
main ()
{
  setup ();


  DrawImage (data, IterMax);	// first find Fatou
  SaveArray2PGMFile (data, iWidth, IterMax, 1, radius, "name = iWidth_IterMax_1_radius  parameter c plane");
  
  PlotPoint(c0, data);
  SaveArray2PGMFile (data, iWidth, IterMax, 2, radius, "name = iWidth_IterMax_2_radius  parameter c plane + c0");

  end ();

  return 0;
}


